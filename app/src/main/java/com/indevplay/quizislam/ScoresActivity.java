package com.indevplay.quizislam;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class ScoresActivity extends Activity {

    private Db db;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_scores);

        db = new Db(this);
        TableLayout table = (TableLayout)findViewById(R.id.scoresLayout);


        table.setGravity(Gravity.TOP);
        table.setGravity(Gravity.CENTER_HORIZONTAL);

        TextView rank = new TextView(this);
        rank.setText("PERINGKAT");
        rank.setPadding(8,8,8,8);
        rank.setTextColor(Color.WHITE);
        rank.setTextSize(20);
        TextView percentage = new TextView(this);
        percentage.setText("SKOR");
        percentage.setTextColor(Color.WHITE);
        percentage.setTextSize(20);
        percentage.setPadding(8,8,8,8);
        TextView score = new TextView(this);
        score.setText("TANGGAL");
        score.setTextColor(Color.WHITE);
        score.setTextSize(20);
        score.setPadding(8,8,8,8);

        TableRow rowHeader = new TableRow(this);

        rowHeader.addView(rank);  //Line 39
        rowHeader.addView(percentage);
        rowHeader.addView(score);

        table.addView(rowHeader);
        Cursor curScore = db.getScores();
        int no = 1;
        if(curScore !=null){
            Log.i("OnCreate","Total: "+curScore.getCount());
            if(curScore.moveToFirst()){

                //return;
                do{

                    TableRow row = new TableRow(this);
                    TextView txtNumber = new TextView(this);
                    TextView txtScor = new TextView(this);
                    TextView txtDate = new TextView(this);

                    int scor = curScore.getInt(curScore.getColumnIndex("score"));
                    String dateScor = curScore.getString(curScore.getColumnIndex("date")) ;

                    txtNumber.setText(Integer.toString(no));
                    txtScor.setText(Integer.toString(scor));
                    txtDate.setText(dateScor);

                    txtNumber.setTextColor(Color.WHITE);
                    txtScor.setTextColor(Color.WHITE);
                    txtDate.setTextColor(Color.WHITE);
                    txtNumber.setTextSize(18);
                    txtScor.setTextSize(18);
                    txtDate.setTextSize(18);


                    Log.i("QuizActivity", "Scor = " + scor + " ,date =  " +dateScor+",no = "+no );


                    row.addView(txtNumber);
                    row.addView(txtScor);
                    row.addView(txtDate);

                    table.addView(row);
                    no++;
                }while(curScore.moveToNext());
            }
        }



	}

    protected void onStart(){
        super.onStart();


    };




}
