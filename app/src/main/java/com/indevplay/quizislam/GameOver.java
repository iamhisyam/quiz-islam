package com.indevplay.quizislam;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.ads.*;

public class GameOver extends Activity {

    InterstitialAd interstitial;
	private TextView t1;
	private TextView t2;
    int score;
    private  Db db;
    CallbackManager callbackManager;
    ShareDialog shareDialog;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        FacebookSdk.sdkInitialize(getApplicationContext());
		setContentView(R.layout.activity_game_over);
		Intent intent = getIntent();
		String statusGame = intent.getStringExtra(QuizActivity.EXTRA_MESSAGE);
		score =  intent.getIntExtra(QuizActivity.EXTRA_SCORE,0);
		
		t1 = (TextView)findViewById(R.id.score);
		t1.setText(Integer.toString(score));
		db = new Db(this);
        db.insertScore(score);
		t2 = (TextView)findViewById(R.id.message);
		t2.setText(statusGame);

        //facebook stuff


       ShareButton shareButton = (ShareButton)findViewById(R.id.fb_share_button);
//        ShareLinkContent content = new ShareLinkContent.Builder()
//                .setContentTitle("Saya mendapatkan Skor "+score+" di Game Quiz Islam! Berapa Skor kamu?")
//                .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.indevplay.quizislam"))
//                .setContentDescription("Manfaatkan waktu luang mu dengan mengasah kemampuan pengetahuan islam melalui Quiz Islam.")
//                .build();
//        shareButton.setShareContent(content);

        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareToFacebook(score);
            }
        });
	   
	}

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
	
	protected void onDestroy(){
		super.onDestroy();
		
		
		
	};
	
	protected void onStart(){
		super.onStart();
		

	};
	

	
       public void InvokeShare(View v){
           String message = "I get score "+score+" from quiz islam";
           Intent share = new Intent(Intent.ACTION_SEND);
           share.setType("text/plain");
           share.putExtra(Intent.EXTRA_TEXT, message);

           startActivity(Intent.createChooser(share, "Share Your Score"));

       }

        public void StartQuiz(View v){
            Intent intent = new Intent(this,QuizActivity.class);
            startActivity(intent);
            finish();
        }

    public void ShareToFacebook(int score){
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {

            }

        });
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle("Saya mendapatkan Skor "+score+" di Game Quiz Islam! Berapa Skormu?")
                    .setContentDescription(
                            "Manfaatkan waktu luang mu dengan mengasah kemampuan pengetahuan islam melalui Quiz Islam.")
                    .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.indevplay.quizislam"))
                    .build();

            shareDialog.show(linkContent);

        }
    }

	
}
