package com.indevplay.quizislam;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;



public class MainActivity extends Activity {
    InterstitialAd interstitial;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);



        Button btnStart = (Button) findViewById(R.id.btn_start);
        Button btnScore = (Button) findViewById(R.id.btn_score);
        Button btnRule = (Button) findViewById(R.id.btn_rule);



        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

                // Create the interstitial.
        interstitial = new InterstitialAd(this);
        interstitial.setAdUnitId("ca-app-pub-7366078751976453/2560870124");


        // Create ad request.
        AdRequest adRequestInter = new AdRequest.Builder().build();

        // Begin loading your interstitial.
        interstitial.loadAd(adRequestInter);

    }

    public void openRule(View view){
        Intent intent = new Intent(this,com.indevplay.quizislam.RuleActivity.class);
        startActivity(intent);
    }

    public void openScores(View view){
        Intent intent = new Intent(this,com.indevplay.quizislam.ScoresActivity.class);
        startActivity(intent);
        displayInterstitial();
    }

    public void startQuiz(View view){
       Intent intent = new Intent(this,com.indevplay.quizislam.QuizActivity.class);
       startActivity(intent);
    }

    // Invoke displayInterstitial() when you are ready to display an interstitial.
	  public void displayInterstitial() {
	    if (interstitial.isLoaded()) {
	      interstitial.show();
	    }
	  }

}
