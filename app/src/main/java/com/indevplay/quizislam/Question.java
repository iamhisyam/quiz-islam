package com.indevplay.quizislam;

public class Question {
	private int id;
	private String question;
	private String choice1;
	private String choice2;
	private String choice3;
	private int difficulty;
	private String answer;
	private int answered;
	
	public Question() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Question(int id, String question, String choice1, String choice2,
			String choice3, int difficulty, String answer, int answered) {
		super();
		this.id = id;
		this.question = question;
		this.choice1 = choice1;
		this.choice2 = choice2;
		this.choice3 = choice3;
		this.difficulty = difficulty;
		this.answer = answer;
		this.answered = answered;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getChoice1() {
		return choice1;
	}

	public void setChoice1(String choice1) {
		this.choice1 = choice1;
	}

	public String getChoice2() {
		return choice2;
	}

	public void setChoice2(String choice2) {
		this.choice2 = choice2;
	}

	public String getChoice3() {
		return choice3;
	}

	public void setChoice3(String choice3) {
		this.choice3 = choice3;
	}

	public int getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public int getAnswered() {
		return answered;
	}

	public void setAnswered(int answered) {
		this.answered = answered;
	}
	
	
}
