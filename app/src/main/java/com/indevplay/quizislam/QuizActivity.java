package com.indevplay.quizislam;

import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class QuizActivity extends Activity {
	
	public final static String EXTRA_MESSAGE = "com.indevplay.quizislam.MSG_GAMEOVER";
	public final static String EXTRA_SCORE = "com.indevplay.quizislam.MSG_SCORE";
	private Cursor question;
	private Db db;
	private int currentId;
	private String str_question;
	private TextView textView;
	private String str_choice1;
	private String str_choice2;
	private String str_choice3;
	private Button b1;
	private Button b2;
	private Button b3;
	private String str_answer;
	protected TextView time;
	private CountDownTimer cd;
	private int timeLeft;
	private int score;
    InterstitialAd interstitial;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// Set the text view as the activity layout
		db = new Db(this);
		randomQuestion(R.layout.activity_quiz);
        // Create the interstitial.
        interstitial = new InterstitialAd(this);
        interstitial.setAdUnitId("ca-app-pub-7366078751976453/2560870124");


        // Create ad request.
        AdRequest adRequest = new AdRequest.Builder().build();

        // Begin loading your interstitial.
        interstitial.loadAd(adRequest);

		
	}

	protected void onDestroy() {
		super.onDestroy();
		db.updateAllQuestion();
	}

	public void checkAnswer(String answer) {

		String right_answer = this.str_answer;
		Log.i("QuizActivity", "answer = " + answer + ", right answer = "
				+ right_answer);
		if (answer.equals(right_answer)) {
			
			score += timeLeft;
			Log.i("QuizActivity","Score = "+score+" ,timeLeft =  "+timeLeft);
			cd.cancel();
			db.updateQuestion(currentId);
			randomQuestion(R.layout.activity_quiz);
		} else {
            cd.cancel();
			db.updateAllQuestion();
			Intent intent = new Intent(this,GameOver.class);
			intent.putExtra(EXTRA_MESSAGE, "Kuis Selesai");
			intent.putExtra(EXTRA_SCORE, score);
			startActivity(intent);
            finish();
            displayInterstitial();
			
		}
	}

	public void randomQuestion(int id) {

		question = db.getQuestions();
		Log.i("QuizActivity", "Jumlah kolom : " + question.getColumnCount());

		if (question.getCount()==0) {		
			Intent intent = new Intent(this,GameOver.class);
			intent.putExtra(EXTRA_MESSAGE, "SELAMAT ANDA TELAH MENYELESAIKAN SEMUA SOAL!");
			intent.putExtra(EXTRA_SCORE, score);
			startActivity(intent);
            finish();
            displayInterstitial();
		} else {

			currentId = question.getInt(8);
			str_question = question.getString(1);
			str_choice1 = question.getString(2);
			str_choice2 = question.getString(3);
			str_choice3 = question.getString(4);
			str_answer = question.getString(6);

			final String[] randChoice = { str_choice1, str_choice2, str_choice3 };
			setContentView(id);
			countTime(20000);
			textView = (TextView) findViewById(R.id.question);
			textView.setText(str_question);

			b1 = (Button) findViewById(R.id.btn_choice1);
			b2 = (Button) findViewById(R.id.btn_choice2);
			b3 = (Button) findViewById(R.id.btn_choice3);
			shuffleArray(randChoice);

			b1.setText(randChoice[0]);
			b2.setText(randChoice[1]);
			b3.setText(randChoice[2]);

			b1.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					checkAnswer(randChoice[0]);
				}
			});
			b2.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					checkAnswer(randChoice[1]);
				}
			});
			b3.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					checkAnswer(randChoice[2]);
				}
			});
		}

	}

	public void countTime(long timeDefault) {
		cd = new CountDownTimer(timeDefault, 1000) {
			long second;

			public void onTick(long millisUntilFinished) {
				time = (TextView) findViewById(R.id.time);
				second = millisUntilFinished / 1000;
				time.setText(Long.toString(second));
				timeLeft = (int)second;
			}

			public void onFinish() {
                db.updateAllQuestion();
                Intent intent = new Intent(getApplicationContext(),GameOver.class);
                intent.putExtra(EXTRA_MESSAGE, "Kuis Selesai");
                intent.putExtra(EXTRA_SCORE, score);
                startActivity(intent);
                finish();
                displayInterstitial();
			}
		}.start();
	}

	static void shuffleArray(String[] ar) {
		Random rnd = new Random();
		for (int i = ar.length - 1; i > 0; i--) {
			int index = rnd.nextInt(i + 1);
			// Simple swap
			String a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;
		}
	}
    public void displayInterstitial() {
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }
}
