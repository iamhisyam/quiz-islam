package com.indevplay.quizislam;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Db extends SQLiteAssetHelper {
	 private static final String DATABASE_NAME = "quizislam.sqlite";
	    private static final int DATABASE_VERSION = 1;

	    public Db(Context context) {
	        super(context, DATABASE_NAME, null, DATABASE_VERSION);
	    }
	    
	    public Cursor getQuestions() {

			SQLiteDatabase db = getReadableDatabase();
			SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

			//String [] sqlSelect = {"0 _id", "FirstName", "LastName"}; 
			String [] sqlSelect = {"0 _id", "question", "choice1","choice2","choice3","difficulty","answer","answered","id_question"}; 
			String sqlTables = "questions";
			String select = "answered = 0";
			String [] selectArgs = null; 
			String groupBy = null;
			String having = null;
			String  order = "RANDOM()";
			String limit = "1";

			qb.setTables(sqlTables);
			Cursor c = qb.query(db, sqlSelect, select, selectArgs,
					groupBy, having, order,limit);

			c.moveToFirst();
			db.close();
			return c;

		}

        public Cursor getScores(){
            SQLiteDatabase db = getReadableDatabase();
            SQLiteQueryBuilder qb = new SQLiteQueryBuilder();


            String [] sqlSelect = {"0 _id", "score", "date"};
            String sqlTables = "scores";
            String select = null;
            String [] selectArgs = null;
            String groupBy = null;
            String having = null;
            String  order = "score DESC";
            String limit = "10";


            qb.setTables(sqlTables);

            Cursor c = qb.query(db, sqlSelect, select, selectArgs,
                    groupBy, having, order,limit);



            return c;
        }

        public  long insertScore(int score){
            SQLiteDatabase db = getWritableDatabase();
            ContentValues cv = new ContentValues();
            SimpleDateFormat sdf = new SimpleDateFormat("d LLL yy");
            String currentDate = sdf.format(new Date());
            cv.put("score", score);
            cv.put("date",currentDate);
            long rs = db.insert("scores",null, cv);
            return rs;
        }


    public int updateQuestion(int id){
	    	SQLiteDatabase db = getReadableDatabase();
	    	ContentValues cv = new ContentValues();
	    	cv.put("answered", 1);
	    	int rs = db.update("questions", cv, "id_question = "+id, null);
	    	return rs;
	    }
	    
	    public int updateAllQuestion(){
	    	SQLiteDatabase db = getReadableDatabase();
	    	ContentValues cv = new ContentValues();
	    	cv.put("answered", 0);
	    	int rs = db.update("questions", cv, null, null);
	    	db.close();
	    	return rs;
	    }
	    
	    
}
